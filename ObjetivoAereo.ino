#include <Servo.h> 

#define PIN_MOTOR_1 3 
#define PIN_MOTOR_2 5
#define PIN_MOTOR_3 6
#define PIN_MOTOR_4 9
#define MIN_ANCHO_PULSO 1000
#define MAX_ANCHO_PULSO 2000
#define PIN_ANALOG_THROTTLE A0
#define PIN_ANALOG_LEFT_RIGHT A1
#define PIN_ANALOG_FRONT_BACK A2

#define MARGEN_SENSIBILIDAD_STICKS 5


Servo motor1, motor2, motor3, motor4;

int throttle, left_right, front_back;
byte potencia_general = 0;
int trimmer_m1 = 0;
int trimmer_m2 = 0;
int trimmer_m3 = 0;
int trimmer_m4 = 0;

void setup() {  
  motor1.attach(PIN_MOTOR_1,MIN_ANCHO_PULSO,MAX_ANCHO_PULSO);
  motor2.attach(PIN_MOTOR_2,MIN_ANCHO_PULSO,MAX_ANCHO_PULSO);
  motor3.attach(PIN_MOTOR_3,MIN_ANCHO_PULSO,MAX_ANCHO_PULSO);
  motor4.attach(PIN_MOTOR_4,MIN_ANCHO_PULSO,MAX_ANCHO_PULSO);
}

void loop() {
  throttle = analogRead(PIN_ANALOG_THROTTLE);
  left_right = analogRead(PIN_ANALOG_LEFT_RIGHT);
  front_back = analogRead(PIN_ANALOG_FRONT_BACK);
  if(left_right > (511+MARGEN_SENSIBILIDAD_STICKS)){
    moverDerecha(map(left_right, (511+MARGEN_SENSIBILIDAD_STICKS),1023,0,255));
  }
  if(left_right < (511-MARGEN_SENSIBILIDAD_STICKS)){
    moverIzquierda(map(left_right,0,(511-MARGEN_SENSIBILIDAD_STICKS),255,0));
  }
  if(front_back > (511+MARGEN_SENSIBILIDAD_STICKS)){
    moverAdelante(map(left_right, (511+MARGEN_SENSIBILIDAD_STICKS),1023,0,255));
  }
  if(front_back < (511-MARGEN_SENSIBILIDAD_STICKS)){
    moverAtras(map(left_right,0,(511-MARGEN_SENSIBILIDAD_STICKS),255,0));
  }
  if(throttle > (511+MARGEN_SENSIBILIDAD_STICKS)){
    subir(map(throttle, (511+MARGEN_SENSIBILIDAD_STICKS),1023,0,10));
  }
  if(throttle < (511-MARGEN_SENSIBILIDAD_STICKS)){
    moverIzquierda(map(throttle,0,(511-MARGEN_SENSIBILIDAD_STICKS),10,0));
  }
}
void moverIzquierda(byte angulo){
  motor2.write((byte)(trimmer_m2 + potencia_general + angulo));
  motor4.write((byte)(trimmer_m4 + potencia_general + angulo));  
}
void moverDerecha(byte angulo){
  motor1.write((byte)(trimmer_m1 + potencia_general + angulo));
  motor3.write((byte)(trimmer_m3 + potencia_general + angulo));  
}
void moverAdelante(byte angulo){
  motor3.write((byte)(trimmer_m3 + potencia_general + angulo));
  motor4.write((byte)(trimmer_m4 + potencia_general + angulo));  
}
void moverAtras(byte angulo){  
  motor1.write((byte)(trimmer_m1 + potencia_general + angulo));  
  motor2.write((byte)(trimmer_m2 + potencia_general + angulo));
}
void subir(byte angulo){
  if((potencia_general + angulo)>255){
    potencia_general = 255;    
  }else{
    potencia_general += angulo;
  }
}
void bajar(byte angulo){
  if((potencia_general - angulo)>0){
    potencia_general -= angulo;   
  }else{
    potencia_general = 0;    
  }
}

